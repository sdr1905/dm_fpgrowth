﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Algorithm.Apriori
{
    public class AssociationRule
    {
        public double Confidence { get; set; }
        public List<FPNode> From { get; set; }
        public List<FPNode> To { get; set; }
        public AssociationRule()
        {
            From = new List<FPNode>();
            To = new List<FPNode>();
        }
    }
}
