﻿using FrequentPatternMining.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Algorithm
{
    public class FPGrowth
    {
        public int OriginalTransactionCount = 0;
        public FPNode ConditionalNode = null;
        // public List<HeaderItem> ConditionalHeaders = new List<HeaderItem>();
        /// <summary>
        /// Original categories order
        /// </summary>
        private List<string> OriginalCategoriesOrder = new List<string>();
        private List<List<bool>> Transactions = new List<List<bool>>();
        /// <summary>
        /// Categories
        /// </summary>
        public List<HeaderItem> OrderedHeadersTable = new List<HeaderItem>();
        private double MinimumSupportCount = 0;
        public FPGrowth(List<HeaderItem> headerItems, List<List<bool>> transactions, double minimumSupportCount)
        {
            OriginalTransactionCount = transactions.Count;
            this.MinimumSupportCount = minimumSupportCount;
            this.Transactions = transactions;
            foreach (HeaderItem hi in headerItems)
            {
                OriginalCategoriesOrder.Add(hi.Name);
            }
            OrderedHeadersTable = headerItems.Where(x => x.SupportCount >= MinimumSupportCount).ToList();
            SortCategoriesByFrequency();
        }
        public FPGrowth CreateChild(List<HeaderItem> headerItems, List<List<bool>> transactions, double minimumSupportCount)
        {
            FPGrowth fpg = new FPGrowth(headerItems, transactions, minimumSupportCount);
            fpg.OriginalTransactionCount = this.OriginalTransactionCount;
            // fpg.ConditionalHeaders = this.ConditionalHeaders;
            return fpg;
        }
        /// <summary>
        /// Sort header table by support count, call this method before perform FP-Growth
        /// </summary>
        private void SortCategoriesByFrequency()
        {
            OrderedHeadersTable = OrderedHeadersTable.OrderByDescending(x => x.Frequency).ToList();
        }
        private List<bool> SortTransactionByCategoriesOrder(List<bool> tran)
        {
            List<bool> resTran = new List<bool>();
            foreach (HeaderItem header in OrderedHeadersTable)
            {
                int originalIndex = OriginalCategoriesOrder.IndexOf(header.Name);
                resTran.Add(tran[originalIndex]);
            }
            return resTran;
        }
        private FPNode GetPreviousOccurency(int itemIndex)
        {
            var header = OrderedHeadersTable[itemIndex];
            FPNode item = header.FirstOccurence;
            while (item != null)
            {
                if (item.Occurency != null)
                {
                    item = item.Occurency;
                }
                else
                {
                    break;
                }
            }
            return item;
        }
        private void AddTransactionToFPTree(List<bool> orderedTransaction, FPNode rootNode, int recursionIndex)
        {
            if (recursionIndex == orderedTransaction.Count)
            {
                return;
            }

            int i = recursionIndex;
            HeaderItem headerItem = OrderedHeadersTable[i];
            bool itemAppeared = orderedTransaction[i];
            FPNode child = null;
            if (itemAppeared)
            {
                child = rootNode.Childs.FirstOrDefault(x => x.Name == headerItem.Name);
                if (child == null)
                {
                    child = new FPNode() { Name = headerItem.Name, Frequency = 1, Parent = rootNode };
                    rootNode.Childs.Add(child);

                    // track occurency item for faster tracing later, 
                    // be notice that we only need a track whenever an item is added into childs collection of current node, 
                    // since it denotes that we now have a difference branch in the tree
                    FPNode previousOccurency = GetPreviousOccurency(recursionIndex);
                    if (previousOccurency == null)
                    {
                        headerItem.FirstOccurence = child;
                    }
                    else
                    {
                        // if (previousOccurency != child)
                        {
                            previousOccurency.Occurency = child;
                        }
                    }
                }
                else
                {
                    child.Frequency += 1;
                }
            }
            else
            {
                child = rootNode;
            }
            // recursively call for child, now root not is current processed node
            AddTransactionToFPTree(orderedTransaction, child, recursionIndex + 1);
        }
        /// <summary>
        /// Construct FP-Tree
        /// </summary>
        /// <param name="minimumSupportCount">Minimum support count, from 0 to 1</param>
        /// <returns></returns>
        public FPTree CreateFPTree()
        {
            // initialize tree
            FPTree tree = new FPTree();
            for (int i = 0; i < Transactions.Count; i++)
            {
                List<bool> tran = Transactions[i];
                // sort items in transaction in order of support count
                tran = SortTransactionByCategoriesOrder(tran);
                AddTransactionToFPTree(tran, tree.Root, 0);
            }
            return tree;
        }
        private List<FPElement> RetrievePrefixTransactionFromNode(FPNode node)
        {
            List<FPElement> res = new List<FPElement>();
            for (int i = 0; i < OrderedHeadersTable.Count - 1; i++)
            {
                res.Add(new FPElement() { Name = OrderedHeadersTable[i].Name });
            }
            FPNode parent = node;
            if (parent == null || parent.IsRoot())
            {
                res.Clear();
            }
            else
            {
                while (parent != null && !parent.IsRoot())
                {
                    int nodeIndexInTransaction = OrderedHeadersTable.IndexOf(OrderedHeadersTable.First(x => x.Name == parent.Name));
                    res[nodeIndexInTransaction].Name = parent.Name;
                    res[nodeIndexInTransaction].Appeared = true;
                    parent = parent.Parent;
                }

            }
            return res;
        }
        public List<List<bool>> GenerateConditionalPatternBase(FPTree tree, FPNode conditionNode, out List<HeaderItem> conditionalHeaders)
        {
            conditionalHeaders = new List<HeaderItem>();
            List<List<bool>> conditionalTransactions = new List<List<bool>>();
            // Count - 1 mean that now we have maximum Count - 1 category since the suffix of the transaction item path 
            // always is the item we are processing
            for (int i = 0; i < OrderedHeadersTable.Count - 1; i++)
            {
                HeaderItem hi = new HeaderItem()
                {
                    Name = OrderedHeadersTable[i].Name
                };
                conditionalHeaders.Add(hi);
            }

            FPNode node = conditionNode;
            while (node != null)
            {
                var prefixPath = RetrievePrefixTransactionFromNode(node.Parent);
                if (prefixPath.Count > 0)
                {
                    // duplicate items in transaction by node's support count
                    // this is the simplest way since we dont need to store the support count any elsewhere
                    // but this also take some cost for duplicated items in transaction
                    for (int j = 0; j < node.Frequency; j++)
                    {
                        conditionalTransactions.Add(prefixPath.Select(x => x.Appeared).ToList());
                    }

                }
                node = node.Occurency;
            }
            // calculate frequency
            foreach (List<bool> tran in conditionalTransactions)
            {
                for (int i = 0; i < conditionalHeaders.Count; i++)
                {
                    conditionalHeaders[i].Frequency += tran[i] ? 1 : 0;
                }
            }
            // 
            foreach (HeaderItem condHeader in conditionalHeaders)
            {
                condHeader.SupportCount = (double)condHeader.Frequency / OriginalTransactionCount;
            }
            return conditionalTransactions;
        }

        #region MAIN METHOD
        public IEnumerable<IEnumerable<FPNode>> GenerateFrequentPatterns(double minSup)
        {
            FPTree tree = this.CreateFPTree();
            return GenerateFrequentPatterns(tree, this, minSup, null);
        }
        private IEnumerable<IEnumerable<FPNode>> GenerateFrequentPatterns(FPTree tree, FPGrowth fpgrowth, double minSup, List<HeaderItem> conditionalItems)
        {
            if (conditionalItems == null)
            {
                conditionalItems = new List<HeaderItem>();
            }
            List<FPNode> conditionNodes = new List<FPNode>();
            conditionalItems.ForEach(x => conditionNodes.Add(new FPNode() { Name = x.Name, Frequency = x.Frequency }));
            if (conditionNodes.Any())
            {
                yield return conditionNodes;
            }

            if (!tree.IsMultipleBranch())
            {
                // get list item in tree, generate all combinations, then concatenate with leaf item in tree
                List<FPNode> nodes = new List<FPNode>();
                FPNode root = tree.Root;
                while (root != null && root.Childs.Count > 0)
                {
                    FPNode child = root.Childs[0]; // collecting node path
                    nodes.Add(child);
                    root = child;
                }
                var combinations = CombinationUtils.GenerateCombinationWithoutRepeat(nodes);
                {
                    //List<FPNode> conditionNodes = new List<FPNode>();
                    //conditionalItems.ForEach(x => conditionNodes.Add(new FPNode() { Name = x.Name, Frequency = x.Frequency }));
                    //if (conditionNodes.Any())
                    //{
                    //    yield return conditionNodes;
                    //}

                    int minimumFreq = int.MaxValue;
                    foreach (var item in combinations)
                    {
                        var i = item;
                        minimumFreq = i.Min(x => x.Frequency);
                        var itemConditionalNodes = conditionNodes.Select(x => new FPNode() { Name = x.Name, Frequency = minimumFreq }).ToList();

                        // set frequency = minimum frequency of items in combination
                        i.ForEach(x => x.Frequency = minimumFreq);
                        yield return i.Concat(itemConditionalNodes);
                    }
                }
            }
            // if tree is multiple branch, recursive execution
            else
            {
                for (int i = fpgrowth.OrderedHeadersTable.Count - 1; i >= 0; i--)
                {
                    HeaderItem hi = fpgrowth.OrderedHeadersTable[i];
                    double itemSupport = (double)hi.Frequency / fpgrowth.OriginalTransactionCount;
                    List<HeaderItem> conditionalHeaders;
                    FPNode conditionNode = hi.FirstOccurence;
                    var list = fpgrowth.GenerateConditionalPatternBase(tree, conditionNode, out conditionalHeaders);
                    var childFpGrow = fpgrowth.CreateChild(conditionalHeaders, list, minSup);
                    List<HeaderItem> recursisionHeaderItems = conditionalItems.ToList();

                    var nm = this.OrderedHeadersTable.First(x => x.Name == "nm");

                    recursisionHeaderItems.Add(Clone(hi));
                 //   recursisionHeaderItems.ForEach(x => x.Frequency = hi.Frequency);
                    FPTree conditionalTree = childFpGrow.CreateFPTree();
                    var recusionItems = GenerateFrequentPatterns(conditionalTree, childFpGrow, minSup, recursisionHeaderItems);

                    //var conditionNodes = conditionalItems.Select(x => new FPNode() { Name = x.Name, Frequency = x.Frequency });
                    //if (conditionNodes.Any())
                    //    yield return conditionNodes;

                    if (recusionItems.Any())
                    {
                        foreach (var item in recusionItems)
                        {
                            yield return item;
                            //var combined = item.Concat(conditionNodes).ToList();
                            //var minFreq = combined.Min(x => x.Frequency);
                            //combined.ForEach(x => x.Frequency = minFreq);
                            //yield return combined;
                        }
                    }
                }
            }
        }
        private HeaderItem Clone(HeaderItem hi)
        {
            return new HeaderItem()
            {
                Frequency = hi.Frequency,
                Name = hi.Name,
                FirstOccurence = hi.FirstOccurence,
                SupportCount = hi.SupportCount
            };
        }
        #endregion
    }
}
