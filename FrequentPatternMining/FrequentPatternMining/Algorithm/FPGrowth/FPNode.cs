﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Algorithm
{
    public class FPNode : ICloneable
    {
        public string Name { get; set; }
        public List<FPNode> Childs { get; set; }
        public FPNode Parent { get; set; }
        public FPNode Occurency { get; set; }
        public int Frequency { get; set; }
        public FPNode()
        {
            Childs = new List<FPNode>();
        }
        public bool IsRoot()
        {
            return this.Name == null;
        }
        public object Clone()
        {
            FPNode node = new FPNode() {Frequency = this.Frequency, Name = this.Name };
            return node;
        }
    }
}
