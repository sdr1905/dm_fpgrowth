﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Algorithm
{
    public class HeaderItem
    {
        /// <summary>
        /// Category name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Frequency in the transaction
        /// </summary>
        public int Frequency { get; set; }
        /// <summary>
        /// Frequency / transaction count (%)
        /// </summary>
        public double SupportCount { get; set; }
        /// <summary>
        /// The first occured item in transaction
        /// </summary>
        public FPNode FirstOccurence { get; set; }
    }
}
