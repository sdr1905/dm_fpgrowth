﻿using FrequentPatternMining.Algorithm;
using FrequentPatternMining.Algorithm.Apriori;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 5)
            {
                PrintMenu();
                return;
            }

            // parse parameters
            try
            {
                string inCsvFile = args[0];
                Out(inCsvFile);
                string outFLfile = args[1];
                string outARfile = args[2];
                double minSup = double.Parse(args[3]);
                double minConf = double.Parse(args[4]);
                List<HeaderItem> headers;
                Out("Reading data...");
                List<List<bool>> transactions = Utils.IOUtils.ReadTransaction(inCsvFile, out headers);
                Out("Generating frequent pattern with min support " + minSup);
                List<FrequentItemSet<FPNode>> frequentPatterns = GenerateFrequentItemSet(headers, transactions, minSup);
                // generate association rules
                frequentPatterns = frequentPatterns.OrderBy(x => x.Items.Count).ToList();
                Out("Generating association rules with min confidence " + minConf);
                Dictionary<int, IEnumerable<AssociationRule>> assRules = GenerateAssociationRules(frequentPatterns, minConf);
                Out("Writing output file...");
                Out("Writing " + frequentPatterns.Count + " frequent item set to " + outFLfile);
                WriteFLToFile(frequentPatterns, transactions.Count, outFLfile);
                Out("Writing " + assRules.Count + " groups association rules to " + outARfile);
                WriteARToFile(assRules, outARfile);
                Out("COMPLETED.");
                Console.Out.Flush();
            }
            catch (Exception ex)
            {
                Out("An error occured: \n\t" + ex.Message);
                Out("Program is terminated.");
                return;
            }
            Environment.Exit(0);
        }
        static Dictionary<int, IEnumerable<AssociationRule>> GenerateAssociationRules(List<FrequentItemSet<FPNode>> frequentPattern, double minConf)
        {
            Dictionary<int, IEnumerable<AssociationRule>> rules = new Dictionary<int, IEnumerable<AssociationRule>>();
            if (frequentPattern.Count > 0)
            {
                Apriori ap = new Apriori(frequentPattern);
                int maxItemSetCount = frequentPattern[frequentPattern.Count - 1].Items.Count;
                for (int i = 2; i <= maxItemSetCount; i++)
                {
                    var asRules = ap.GenerateAssociationRules(minConf, i);
                    if (asRules.Any())
                    {
                        rules.Add(i, asRules);
                    }
                }
            }
            return rules;
        }
        static List<FrequentItemSet<FPNode>> GenerateFrequentItemSet(List<HeaderItem> headers, List<List<bool>> transactions, double minSup)
        {
            FPGrowth algorithm = new FPGrowth(headers, transactions, minSup);
            var frequentPatterns = algorithm.GenerateFrequentPatterns(minSup).ToList();
            // copy from frequent collection to list FrequentItemSet
            List<FrequentItemSet<FPNode>> itemSet = new List<FrequentItemSet<FPNode>>();
            foreach (IEnumerable<FPNode> item in frequentPatterns)
            {
                FrequentItemSet<FPNode> fpi = new FrequentItemSet<FPNode>();
                fpi.Items = item.ToList();
                fpi.Frequency = fpi.Items[0].Frequency;
                itemSet.Add(fpi);
            }
            return itemSet;
        }
        static void WriteARToFile(Dictionary<int, IEnumerable<AssociationRule>> asRules, string outFile)
        {
            if (File.Exists(outFile))
            {
                File.Delete(outFile);
            }
            StreamWriter sw = new StreamWriter(outFile);
            foreach (KeyValuePair<int, IEnumerable<AssociationRule>> kv in asRules)
            {
                sw.WriteLine(kv.Value.Count());
                // write out association rules were generated from k-frequent item set
                foreach (AssociationRule ar in kv.Value)
                {
                    sw.Write(string.Format("{0:0.00}", ar.Confidence));
                    sw.Write("    ");
                    ar.From.ForEach(x => sw.Write(x.Name + " "));
                    sw.Write("---> ");
                    ar.To.ForEach(x => sw.Write(x.Name + " "));
                    sw.WriteLine();
                }
            }
            sw.Flush();
            sw.Close();
        }
        /// <summary>
        /// frequent item set must be sorted first
        /// </summary>
        static void WriteFLToFile(List<FrequentItemSet<FPNode>> frequentItemSet, int totalTransactionCount, string outFile)
        {
            if (File.Exists(outFile))
            {
                File.Delete(outFile);
            }
            StreamWriter sw = new StreamWriter(outFile);
            var grouped = frequentItemSet.GroupBy(x => x.Items.Count);
            foreach (var group in grouped)
            {
                // write total k-frequent items
                sw.WriteLine(group.Count());
                foreach (var fqItem in group)
                {
                    // write min sup
                    double support = (double)fqItem.Items[0].Frequency / totalTransactionCount;
                    sw.Write(string.Format("{0:0.00}", support));
                    sw.Write("    ");
                    fqItem.Items.ForEach(x => sw.Write(" " + x.Name));
                    sw.WriteLine();
                }
            }
            sw.Flush();
            sw.Close();
        }
        static void PrintMenu()
        {
            Out("Syntax: ");
            Out("23_1A <IN FILE PATH> <FI OUT FILE PATH> <AR OUT FILE PATH> <min support> <min confidence>");
            Out("Example:\\23_1A input.csv outFI.txt outAR.txt 0.6 0.8");
        }
        static void Out(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
