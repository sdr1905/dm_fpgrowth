﻿using FrequentPatternMining.Algorithm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Utils
{
    public class CombinationUtils
    {

        public static int GetCombinationCount(int n, int k)
        {
            int sum = Factorial(n) / (Factorial(k) * (Factorial(n - k)));
            return sum;
        }
        public static int Factorial(int n)
        {
            int sum = 1;
            for (int i = 1; i <= n; i++)
            {
                sum *= i;
            }
            return sum;
        }

        //public List<List<FPNode>> GenerateFrequentPatterns(
        public static List<List<T>> GenerateCombinationWithoutRepeat<T>(List<T> source)
        {
            List<List<T>> res = new List<List<T>>();
            for (int i = 1; i <= source.Count; i++)
            {
                foreach (IEnumerable<T> lst in GenerateCombinationWithoutRepeat(source, i))
                {
                    res.Add(lst.ToList());
                }
            }
            return res;
        }
        public static IEnumerable<IEnumerable<T>> GenerateCombinationWithoutRepeat<T>(IEnumerable<T> arr, int k)
        {
            int i = 0;
            foreach (var item in arr)
            {
                if (k == 1)
                {
                    yield return new T[] { item };
                }
                else
                {
                    foreach (var result in GenerateCombinationWithoutRepeat<T>(arr.Skip(i + 1), k - 1))
                    {
                        yield return new T[] { item }.Concat(result);
                    }
                }
                ++i;
            }
        }
    }

}
