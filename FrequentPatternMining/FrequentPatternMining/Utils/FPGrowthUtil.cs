﻿using FrequentPatternMining.Algorithm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Utils
{
    public static class FPGrowthUtil
    {
        public static List<HeaderItem> GetHeaders(DataTable hdTable)
        {
            List<HeaderItem> headers = new List<HeaderItem>();
            foreach (DataRow row in hdTable.Rows)
            {
                HeaderItem hi = new HeaderItem();
                hi.Name = row[0].ToString();
                headers.Add(hi);
            }
            return headers;
        }
        public static List<List<bool>> ReadTransaction(DataSet transactionInfo, out List<HeaderItem> headerItems)
        {
            headerItems = GetHeaders(transactionInfo.Tables[0]);
            List<List<bool>> transactions = new List<List<bool>>();
            foreach(DataRow row in transactionInfo.Tables[1].Rows)
            {
                bool[] transaction = Enumerable.Repeat(false, headerItems.Count).ToArray();
                string itemNames = row[0].ToString();
                string[] tks = itemNames.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in tks)
                {
                    HeaderItem header = headerItems.First(x => x.Name == item);
                    int itemIndex = headerItems.IndexOf(header);
                    if (itemIndex > -1)
                    {
                        transaction[itemIndex] = true;
                        header.Frequency++;
                    }
                }
                transactions.Add(transaction.ToList());
            }
            headerItems.ForEach(x => x.Support = (double)x.Frequency / transactions.Count);
            return transactions;
        }
    }
}
