﻿using FrequentPatternMining.Algorithm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Utils
{
    public static class IOUtils
    {
        public static List<List<bool>> ReadTransaction(string csvFile, out List<HeaderItem> headerItems)
        {
            headerItems = new List<HeaderItem>();
            List<List<bool>> transactions = new List<List<bool>>();
            StreamReader sr = new StreamReader(csvFile);
            if (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                string[] hdrs = line.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string h in hdrs)
                {
                    headerItems.Add(new HeaderItem()
                    {
                        Name = h.Trim(),
                        Frequency = 0,
                        FirstOccurence = null
                    });
                }
            }

            while (!sr.EndOfStream)
            {
                List<bool> tran = new List<bool>();
                string line = sr.ReadLine();
                if (string.IsNullOrEmpty(line))
                {
                    continue;
                }
                string[] items = line.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < items.Length; i++)
                {
                    bool appeared = items[i].Trim() == "y";
                    if (appeared)
                    {
                        headerItems[i].Frequency++;
                    }
                    tran.Add(appeared);
                }
                transactions.Add(tran);
            }
            headerItems.ForEach(x => x.SupportCount = (double)x.Frequency / transactions.Count);
            return transactions;
        }
    }
}
