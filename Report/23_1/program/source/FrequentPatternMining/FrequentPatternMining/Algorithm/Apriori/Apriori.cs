﻿using FrequentPatternMining.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Algorithm.Apriori
{
    public class Apriori
    {
        private List<FrequentItemSet<FPNode>> FrequentItems { get; set; }
        public Apriori(List<FrequentItemSet<FPNode>> frequentItemSet)
        {
            FrequentItems = frequentItemSet;
        }
        private int FindFrequencyFromItemSet(IEnumerable<FrequentItemSet<FPNode>> sourceItemSet, IEnumerable<FPNode> sourceToFindItemSet)
        {
            var sortedITemSource = sourceToFindItemSet.OrderBy(x => x.Name);
            int sortedItemCount = sortedITemSource.Count();
            int frequent = int.MinValue;
            foreach (FrequentItemSet<FPNode> item in sourceItemSet)
            {
                if (item.Items.Count == sortedItemCount)
                {
                    var sortedItems = item.Items.OrderBy(x => x.Name).ToList();
                    int i = 0;
                    foreach (FPNode sis in sortedITemSource)
                    {
                        if (sis.Name != sortedItems[i].Name)
                        {
                            break;
                        }
                        else
                        {
                            // reach the last element
                            if (i == item.Items.Count - 1)
                            {
                                frequent = sortedItems[i].Frequency;
                            }
                        }
                        i++;
                    }
                }
            }
            return frequent;
        }
        /// <summary>
        /// k >= 2
        /// </summary>
        public IEnumerable<AssociationRule> GenerateAssociationRules(double minConf, int fromkItems)
        {
            // loop all k-itemset in frequent items, k>=2, call as F
            // get all k-1 frequent items set
            foreach (var item in this.FrequentItems)
            {
                FrequentItemSet<FPNode> itemSet = item;
                if (itemSet.Items.Count == fromkItems)
                {
                    // generate all nonempty subset of F, call as S
                    for (int i = 1; i <= fromkItems - 1; i++)
                    {
                        var prevFrequentItems = this.FrequentItems.Where(x => x.Items.Count == i);
                        int setFrequency = itemSet.Items[0].Frequency;
                        IEnumerable<IEnumerable<FPNode>> subset = CombinationUtils.GenerateCombinationWithoutRepeat(itemSet.Items, i);
                        foreach (var cbi in subset)
                        {
                            // compute confidence
                            // with each item S, let L = F-S
                            // conf = support(F) / support(S)
                            // if conf is greater or equal than min_conf, S -> L is a strong association rule
                            int frequent = FindFrequencyFromItemSet(prevFrequentItems, cbi);

                            if (frequent != int.MinValue)
                            {
                                double conf = (double)setFrequency / frequent;
                                if (conf >= minConf)
                                {
                                    AssociationRule ar = new AssociationRule();
                                    ar.Confidence = conf;
                                    ar.From = cbi.ToList();
                                    ar.To = itemSet.Items.Where(x => !ar.From.Contains(x)).ToList();
                                    yield return ar;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
