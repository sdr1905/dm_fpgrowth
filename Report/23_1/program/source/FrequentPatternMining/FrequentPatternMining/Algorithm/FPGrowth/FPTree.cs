﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Algorithm
{
    public class FPTree
    {
        public FPNode Root { get; set; }
        public FPTree()
        {
            this.Root = new FPNode();
            this.Root.Parent = null;
            this.Root.Name = null;
        }
        public bool IsMultipleBranch()
        {
            FPNode n = this.Root;
            while (n.Childs.Count > 0)
            {
               if (n.Childs.Count > 1)
                {
                    return true;
                }
                n = n.Childs[0];
            }
            return false;
        }
    }
}
