﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Algorithm
{
    public class FrequentItemSet <T>
    {
        public int Frequency { get; set; }
        public List<T> Items { get; set; }
        public FrequentItemSet()
        {
            this.Items = new List<T>();
        }
    }
}
