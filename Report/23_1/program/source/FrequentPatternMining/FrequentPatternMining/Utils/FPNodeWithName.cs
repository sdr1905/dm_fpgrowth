﻿using FrequentPatternMining.Algorithm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrequentPatternMining.Utils
{
    /// <summary>
    /// Util class for debugging
    /// </summary>
    public class FPElement
    {
        public bool Appeared { get; set; }
        public string Name { get; set; }
    }
}
